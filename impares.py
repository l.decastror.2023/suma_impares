
seguir = True
acumulador = 0
while seguir:
    try:
        numero1 = int(input("Dame un numero entero no negativo: "))
        numero2 = int(input("Dame otro numero entero no negativo: "))
        if numero1 > 0 or numero2 > 0:
            seguir = False
        else:
            print("Dame un numero positivo ")
    except:
        print("Dame un numero entero")
for i in range(numero1, numero2 + 1):
    if i % 2 != 0:
        acumulador += i

print("La suma de los impares comprendidos entre", numero1, "y", numero2, "es:", acumulador)